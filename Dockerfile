FROM ubuntu:focal

ENV TZ=America/New_York
ENV DEBIAN_FRONTEND="noninteractive"
ENV AWESOME="true"

RUN  TZ=America/New_York DEBIAN_FRONTEND="noninteractive" apt-get -y update && apt-get -y  upgrade && apt-get -y install git golang python3 curl 
RUN curl --output /tmp/hugo.deb -sSL https://github.com/gohugoio/hugo/releases/download/v0.74.3/hugo_extended_0.74.3_Linux-64bit.deb
# COPY hugo_0.70.0_Linux-64bit.deb  /tmp/hugo.deb
RUN dpkg -i /tmp/hugo.deb


COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s /usr/local/bin/docker-entrypoint.sh / # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]

WORKDIR /src

EXPOSE 1313
